Role Name
=========

Simple role to set up Prometheus and AlertManager.

Requirements
------------

None, but will most likely want the node exporter module.

Role Variables
--------------

Default variables are fairly self explanatory. You will need these variables in either grou_vars or host_vars though:

    smtp_server:
    smtp_port:
    from_address:
    smtp_username:
    smtp_pass: (use Ansible Vault )

You will also need a list of hosts:

    prometheus_hosts:
      - host1
      - host2
      - host3

Dependencies
------------

none

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - prometheus

License
-------

BSD

Author Information
------------------

John Hooks
